import React, { useEffect, useState } from 'react';
import { Site, Test } from './lib/types/types';
import { getSites, getTests } from './lib/api/api';
import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';
import TestPage from './pages/Test';
import Dashboard from './pages/Dashboard';
import { sortedListByName, sortedListBySite, sortedListByType } from './lib/utils';

function App() {
  const [tests, setTests] = useState<Test[]>([])
  const [sites, setSites] = useState<Site[]>([])
  const [searchInput, setSearchInput] = useState<string>('');
  const [ascending, setAscending] = useState<boolean>(true);

  function handleItemSearch(e: React.ChangeEvent<HTMLInputElement>) {
    setSearchInput(e.target.value);
  }
  
  const filteredItems: Test[] = tests.filter((test: Test) => {
    return test.name.toLowerCase().includes(searchInput.toLowerCase())
  })

  function searchReset() {
    setSearchInput('')
  }

  useEffect(() => {
    getTests()
      .then(res => {
        setTests(res)
      })
      .catch((err) => console.log(err))

    getSites()
    .then(res => {
      setSites(res)
    })
    .catch((err) => console.log(err))
  }, [])

  const handleSortByName = () => {
    setAscending((prevAscending) => !prevAscending);
    const filteredList = sortedListByName(tests, ascending)
    setTests(filteredList);
  };

  const handleSortByType = () => {
    setAscending((prevAscending) => !prevAscending);
    const filteredList = sortedListByType(tests, ascending)
    setTests(filteredList);
  };

  const handleSortBySite = () => {
    setAscending((prevAscending) => !prevAscending);
    const filteredList = sortedListBySite(tests, ascending, sites)
    setTests(filteredList);
  };

  return (
    <Router>
      <Routes>
        <Route path='/' element={
          <Dashboard 
            onClick={searchReset}
            onChange={handleItemSearch}
            tests={filteredItems}
            sites={sites}
            value={searchInput}
            onFilterByName={handleSortByName}
            onFilterByType={handleSortByType}
            onFilterBySite={handleSortBySite}
          />} 
        />
        <Route path='/:state/:id' element={<TestPage />} />
      </Routes>
    </Router>
  );
}

export default App;
