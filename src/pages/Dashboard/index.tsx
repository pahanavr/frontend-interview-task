import Layout from '../../components/layout'
import Plug from '../../components/plug'
import SearchInput from '../../components/searchInput'
import Table from '../../components/table'
import { Site, Test } from '../../lib/types/types'

interface DashboardProps {
  tests: Test[]
  sites: Site[]
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
  onClick: () => void
  value?: string
  onFilterByName: () => void
  onFilterByType: () => void
  onFilterBySite: () => void
}

const Dashboard = ({
  tests,
  sites, 
  onChange, 
  onClick, 
  value,
  onFilterByName,
  onFilterByType,
  onFilterBySite
}: DashboardProps) => {
  return (
    <Layout title='Dashboard'>
      <SearchInput total={tests.length} onChange={onChange} value={value} />
      {tests.length !== 0 ? 
        <Table
          tests={tests} 
          sites={sites} 
          onFilterByName={onFilterByName}
          onFilterByType={onFilterByType}
          onFilterBySite={onFilterBySite}
        />
        : 
        <Plug onClick={onClick} />
      }
    </Layout>
  )
}

export default Dashboard
