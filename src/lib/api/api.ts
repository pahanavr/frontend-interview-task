import { fetchAPI } from '.';
import { Site, Test } from '../types/types';

export async function getSites(): Promise<Site[]> {
  const sites = await fetchAPI(
    'GET',
    'sites'
  )

  return sites
}

export async function getTests(): Promise<Test[]> {
  const tests = await fetchAPI(
    'GET',
    'tests'
  )

  return tests
}

export async function getTest(id: number): Promise<Test> {
  const test = await fetchAPI(
    'GET',
    `tests/${id}`
  )

  return test
}
