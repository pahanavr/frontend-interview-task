import Button from '../button'
import styles from './styles.module.css'

const Plug = ({onClick}: {onClick: () => void}) => {
  return (
    <div className={styles.plug}>
      <h2 className={styles.plug__title}>
        Your search did not match any results
      </h2>
      <Button onClick={onClick} className={styles.plug__button}>
        Reset
      </Button>
    </div>
  )
}

export default Plug
