import styles from './styles.module.css'
import cx from 'classnames'

interface TableTitleProps {
  onFilterByName: () => void
  onFilterByType: () => void
  onFilterBySite: () => void
}

const TableTitle = ({
  onFilterByName,
  onFilterBySite,
  onFilterByType
}: TableTitleProps) => {
  return (
    <div className={styles.tableTitle}>
      <p 
        onClick={onFilterByName}
        className={cx(styles.tableTitle__text, styles.tableTitle__name)}
      >
        NAME
      </p>
      <p 
        onClick={onFilterByType} 
        className={cx(styles.tableTitle__text, styles.tableTitle__type)}
      >
        TYPE
      </p>
      <p 
        className={cx(styles.tableTitle__text, styles.tableTitle__status)}
      >
        STATUS
      </p>
      <p 
        onClick={onFilterBySite} 
        className={cx(styles.tableTitle__text, styles.tableTitle__text_type_url)}
      >
        SITE
      </p>
    </div>
  )
}

export default TableTitle
