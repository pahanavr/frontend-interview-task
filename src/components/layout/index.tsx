import styles from './styles.module.css'
import cx from 'classnames'

interface LayoutProps {
  title: string
  children: JSX.Element | JSX.Element[]
  className?: string
}

const Layout = ({title, children, className}: LayoutProps) => {
  return (
    <div className={cx(styles.page, className)}>
      <header className={styles.page__header}>
        <h1 className={styles.page__title}>
          {title}
        </h1>
      </header>
      {children}
    </div>
  )
}

export default Layout
