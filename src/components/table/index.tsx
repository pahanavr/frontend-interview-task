import { Site, Test } from '../../lib/types/types'
import TableRow from '../tableRow'
import TableTitle from '../tableTitle'
import styles from './styles.module.css'

interface TableProps {
  tests: Test[]
  sites: Site[]
  onFilterByName: () => void
  onFilterByType: () => void
  onFilterBySite: () => void
}

const Table = ({
  tests,
  sites,
  onFilterByName,
  onFilterByType,
  onFilterBySite
}: TableProps) => {
  return (
  <div className={styles.table}>
    <TableTitle
      onFilterByName={onFilterByName}
      onFilterByType={onFilterByType}
      onFilterBySite={onFilterBySite}
    />
    {tests.map((test: Test) => (
      <TableRow key={test.id} test={test} sites={sites}/>
    ))}
  </div>
  )
}


export default Table
